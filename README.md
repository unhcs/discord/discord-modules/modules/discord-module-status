# discord-module-status

## Overview

This module adds support for toggleable Discord roles. Useful for floating users to the top of the user list when online.

## Application commands

Two guild-level application commands are added by this module: online and offline. Executing `/online` will prompt a select menu allowing for the selection of a role in which to go online. Likewise, `/offine` displays all presently-online roles, which one can then toggle offline for. 

## Notice

While in production, this module is very much a work-in-progress. This module was produced for use in the UNH CS Department Discord server and as such, the features are customized for our given use cases. 

If you find bugs or think of ideas for features to add, feel free to drop and issue and we will check it out as soon as possible.
