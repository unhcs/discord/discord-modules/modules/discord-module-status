import Toggleable, {ToggleableTypes} from "../parsing/structs/Toggleable"
import ToggleableRole from "../parsing/structs/ToggleableRole"
import ToggleableRoleGroup from "../parsing/structs/ToggleableRoleGroup"
import Role from "../parsing/structs/Role"

type TransitionStateType = "online" | "offline"

class ToggleablesRepository {
    private toggleables: Toggleable[]

    public constructor(toggleables?: Toggleable[]) {
        this.toggleables = toggleables ?? [];
    }

    public set(toggleables: Toggleable[]) {
        this.toggleables = toggleables;
    }

    public getRoles(discordMemberRoles: string[], state: TransitionStateType) {
        return this.toggleables.filter(t =>
            t.type == ToggleableTypes.ROLE && (this.hasRole(discordMemberRoles, t as ToggleableRole, state))
        ) as ToggleableRole[]
    }

    public getGroups(discordMemberRoles: string[], roles: ToggleableRole[]) {
        return this.toggleables.filter(t =>
            t.type == ToggleableTypes.GROUP && roles.some(r =>
                (t as ToggleableRoleGroup).choices.includes(r.name))
        ) as ToggleableRoleGroup[]
    }

    private oppositeState = (state: TransitionStateType): TransitionStateType => {
        return state === "online" ? "offline" : "online"
    }

    private hasRole(memberRoleNames: string[], role: ToggleableRole, state: TransitionStateType): boolean {
        const roles: Role[] = role.roles[this.oppositeState(state)]

        return roles.every(r => memberRoleNames.includes(r.name) || !r.required)
    }
}

export default ToggleablesRepository;

export {
    TransitionStateType
}