import {
    CommandInteraction,
    GuildMember,
    Interaction,
    MessageActionRow,
    MessageSelectMenu,
    Role as DiscordRole,
    SelectMenuInteraction
} from "discord.js"
import ToggleableRole from "../parsing/structs/ToggleableRole"
import ToggleableRoleGroup from "../parsing/structs/ToggleableRoleGroup"
import {GuildApplicationCommand, GuildApplicationCommandOptions, Listener, Module} from "@unh-csonline/discord-modules"
import ToggleableRolesModule from "../index"
import {SlashCommandBuilder} from "@discordjs/builders"
import Toggleable from "../parsing/structs/Toggleable"
import Role from "../parsing/structs/Role"
import role from "../parsing/structs/Role"
import {I18n} from "i18n"

class OfflineCommand extends GuildApplicationCommand {
    static SelectMenuListener = class extends Listener<"interactionCreate"> {
        constructor(private command: OfflineCommand, module: Module) {
            super(module, "interactionCreate")
        }

        async execute(interaction: Interaction): Promise<void> {
            if (interaction.isSelectMenu() && interaction.customId === "offline") {
                const selection: string = interaction.values[0]
                const toggleableName = selection.slice(2)

                let roleSelection: ToggleableRole | undefined

                if (selection.startsWith("g-")) {
                    const groupSelection: ToggleableRoleGroup | undefined = this.command.groups.find(r => r.name === toggleableName)

                    if (groupSelection == undefined) {
                        return interaction.update({
                            embeds: [
                                {
                                    description: this.command.i18n.__("COMMAND_ERROR/INVALID_STATE/NO_ROLE_SELECTION")
                                }
                            ],
                            components: []
                        }).catch(console.error)
                    }

                    const memberRoles = (interaction.member as GuildMember).roles.cache.map(r => r.name)
                    const repository = (this.module as ToggleableRolesModule).repository

                    // get all possible roles one can go online for
                    const roles = repository.getRoles(memberRoles, "offline")
                    const groups = repository.getGroups(memberRoles, roles)

                    if (roles.filter(r => r.parent === groupSelection.name).length == 1 &&
                        groups.filter(r => r.parent === groupSelection.name).length == 0) {
                        roleSelection = roles[0]
                    }
                }

                if (selection.startsWith("r-") || roleSelection) {
                    roleSelection = roleSelection ?? this.command.roles.find(r => r.name === toggleableName)

                    if (roleSelection == undefined) {
                        return interaction.update({
                            embeds: [
                                {
                                    description: this.command.i18n.__("COMMAND_ERROR/INVALID_STATE/NO_ROLE_SELECTION")
                                }
                            ],
                            components: []
                        }).catch(console.error)
                    }

                    const updateSuccess: boolean = await this.command.applyRolesChange(interaction, roleSelection)

                    if (updateSuccess) {
                        return interaction.update({
                            embeds: [
                                {
                                    description: this.command.i18n.__("COMMAND/OFFLINE/SUCCESS", `${roleSelection.name ?? "Unknown"}`)
                                }
                            ],
                            components: []
                        }).catch(console.error)
                    }

                    return
                } else {
                    return this.command.promptForSelection(interaction, toggleableName)
                }
            }

            return
        }
    }

    private roles: ToggleableRole[]
    private groups: ToggleableRoleGroup[]
    private ids: string[]
    private i18n: I18n

    constructor(module: ToggleableRolesModule) {
        super(module, {
            guild: module.client.guilds.cache.first(),
            data: new SlashCommandBuilder()
                .setName("offline")
                .setDescription("Go inactive for a given role.")
        } as GuildApplicationCommandOptions)

        this.roles = []
        this.groups = []
        this.ids = []
        this.i18n = (this.module as ToggleableRolesModule).i18n
    }

    public execute(interaction: CommandInteraction): Promise<void> {
        return new Promise(async (resolve, reject) => {
            if (!interaction.member) {
                return this.resolveWithError(interaction, this.i18n.__("COMMAND_ERROR/GUILDS_ONLY"))
            }

            const member: GuildMember = interaction.member as GuildMember

            // known: command is being executed in guild

            if (interaction.channel?.type !== "GUILD_TEXT") {
                return this.resolveWithError(interaction, this.i18n.__("COMMAND_ERROR/TEXT_CHANNEL_ONLY"))
            }

            const memberRoles = member.roles.cache.map(r => r.name)
            const repository = (this.module as ToggleableRolesModule).repository

            // get all possible roles one can go online for
            this.roles = repository.getRoles(memberRoles, "offline")

            // get all groups containing one or more choices the member has available
            this.groups = repository.getGroups(memberRoles, this.roles)

            if (this.roles.length + this.groups.length == 0) {
                return this.resolveWithError(interaction, this.i18n.__("COMMAND/OFFLINE/NO_CANDIDATES"))
            } else if (this.roles.length == 1) {
                const updateSuccess: boolean = await this.applyRolesChange(interaction, this.roles[0])

                if (updateSuccess) {
                    return interaction.reply({
                        ephemeral: true,
                        embeds: [
                            {
                                description: this.i18n.__("COMMAND/OFFLINE/SUCCESS", `${this.roles[0].name ?? "Unknown"}`)
                            }
                        ],
                        components: []
                    }).catch(console.error)
                }
            }

            await this.promptForSelection(interaction, null)

            return resolve()
        })
    }

    private async resolveWithError(interaction: CommandInteraction | SelectMenuInteraction, message: string): Promise<void> {
        await interaction.reply({
            ephemeral: true,
            embeds: [
                {
                    description: message
                }
            ]
        }).catch(err => Promise.reject(err))
        return Promise.resolve()
    }

    private promptForSelection(interaction: CommandInteraction | SelectMenuInteraction, parent: string | null): Promise<any> {
        const menu = new MessageSelectMenu().setCustomId("offline")

        const compare = (a: Toggleable, b: Toggleable) => a.name.localeCompare(b.name)

        const roles = this.roles.filter(r => r.parent == parent).sort(compare)
        const groups = this.groups.filter(g => g.parent == parent).sort(compare)

        for (const group of groups) {
            menu.addOptions([
                {
                    label: group.name,
                    value: `g-${group.name}`,
                    emoji: interaction.guild?.emojis.cache.find(e => e.name === "role_group")
                }
            ])
        }

        for (const role of roles) {
            menu.addOptions([
                {
                    label: role.name,
                    value: `r-${role.name}`,
                    emoji: interaction.guild?.emojis.cache.find(e => e.name === "role")
                }
            ])
        }

        const row = new MessageActionRow().addComponents(menu)

        const messageData = {
            ephemeral: true,
            content: this.i18n.__("COMMAND/OFFLINE/SELECTION_PROMPT_DESCRIPTION"),
            components: [row]
        }

        if (interaction.isCommand()) {
            return interaction.reply(messageData).catch(console.error)
        } else {
            return interaction.update(messageData).catch(console.error)
        }
    }

    private async applyRolesChange(interaction: SelectMenuInteraction | CommandInteraction, toggleableRole: ToggleableRole): Promise<boolean> {
        let member = interaction.member as GuildMember

        const offlineRoles: DiscordRole[] = this.getRoles(toggleableRole.roles.offline)
        const onlineRoles: DiscordRole[] = this.getRoles(toggleableRole.roles.online)

        if (offlineRoles.length !== toggleableRole.roles.offline.length ||
            onlineRoles.length !== toggleableRole.roles.online.length) {

            if (interaction.isCommand()) {
                await interaction.reply({
                    content: this.i18n.__("STATE_ERROR/MISSING_ROLES"),
                    components: []
                }).catch(console.error)
            } else {
                await interaction.update({
                    content: this.i18n.__("STATE_ERROR/MISSING_ROLES"),
                    components: []
                }).catch(console.error)
            }

            return Promise.resolve(false)
        }

        try {
            await member.roles.remove(onlineRoles)
                .then(gm => member = gm)
            await member.roles.add(offlineRoles)
        } catch (err) {
            if (interaction.isCommand()) {
                await interaction.reply({
                    content: this.i18n.__("COMMAND_ERROR/ROLE_CHANGE_FAILED"),
                    components: []
                }).catch(console.error)
            } else {
                await interaction.update({
                    content: this.i18n.__("COMMAND_ERROR/ROLE_CHANGE_FAILED"),
                    components: []
                }).catch(console.error)
            }
            return Promise.resolve(false)
        }

        return Promise.resolve(true)
    }

    private getRoles(roleNames: Role[]): DiscordRole[] {
        const discordRoleNames: string[] = roleNames.map(r => r.name)

        return this.options.guild.roles.cache.filter(r => discordRoleNames.includes(r.name)).map(r => r)
    }
}

export default OfflineCommand