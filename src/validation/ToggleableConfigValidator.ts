import {Validator, ValidatorResult} from "jsonschema"

import {
    RoleSchema,
    RolesOptionSchema,
    ToggleableConfigSchema,
    ToggleableOptionSchema
} from "./schemas/ToggleableConfigSchema"
import IValidator from "./IValidator"

class ToggleableConfigValidator implements IValidator {
    private validator: Validator

    constructor() {
        this.validator = new Validator()

        this.validator.addSchema(RoleSchema, "/Role")
        this.validator.addSchema(RolesOptionSchema, "/RolesOption")
        // appears to be an error in the type definitions which disallows a ref inside a then statement
        // @ts-ignore
        this.validator.addSchema(ToggleableOptionSchema, "/ToggleableOption")
    }

    public validate(data: object): ValidatorResult {
        return this.validator.validate(data, ToggleableConfigSchema)
    }

    public isValid(data: object) {
        return this.validate(data).valid
    }
}

export default ToggleableConfigValidator