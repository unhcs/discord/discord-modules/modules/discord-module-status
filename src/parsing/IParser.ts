import IValidator from "../validation/IValidator"

interface IParser<T> {
    parse(data: object): Promise<T[]>;
}

export default IParser