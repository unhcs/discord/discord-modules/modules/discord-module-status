import {ToggleableTypes} from "../structs"
import ConfigToggleableRole from "../structs/ConfigToggleableRole"
import ToggleableRole from "../structs/ToggleableRole"
import RoleTranslation from "./RoleTranslation"

class ToggleableRoleTranslation {
    public static translate(role: ConfigToggleableRole) {
        let roles = {
            online: role.roles.online.map(r => RoleTranslation.translate(r)),
            offline: role.roles.offline.map(r => RoleTranslation.translate(r))
        }

        return {
            name: role.name,
            type: ToggleableTypes.ROLE,
            roles: roles
        } as ToggleableRole
    }
}

export default ToggleableRoleTranslation