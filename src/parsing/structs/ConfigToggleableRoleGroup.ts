import ConfigToggleable from "./ConfigToggleable"

interface ConfigToggleableRoleGroup extends ConfigToggleable {
    type: 1;
    choices: [
        ConfigToggleable
    ]
}

export default ConfigToggleableRoleGroup