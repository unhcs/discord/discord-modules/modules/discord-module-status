import ConfigToggleable from "./ConfigToggleable"
import ConfigRole from "./ConfigRole"

interface ConfigToggleableRole extends ConfigToggleable {
    type: 0;
    roles: {
        online: [
            ConfigRole
        ],
        offline: [
            ConfigRole
        ]
    }
}

export default ConfigToggleableRole